# 婚笺


## 简介

这是一个用小程序原生开发（云开发）的婚礼请柬小程序，由于自己已使用过，故开源给有需要的同学。


## 软件架构

+ 前端：微信小程序原生

+ 后端：微信小程序云

+ 编辑器：微信开发者工具


## 简单预览
![](https://ae01.alicdn.com/kf/U9e6920edcfb34daa929456876cc910416.jpg) ![](https://ae01.alicdn.com/kf/U528ea6ab50cc412bb817f43b12edec25r.jpg) ![](https://ae01.alicdn.com/kf/Ubb69554fb2864482a63de5fc08cd13abg.jpg) ![](https://ae01.alicdn.com/kf/U8ab5fe2e4fba461d998b1f45ae7414868.jpg) ![](https://ae01.alicdn.com/kf/U98d62f88a15c469d9f3aadd4571153213.jpg) ![](https://ae01.alicdn.com/kf/Ude8eac2b805a4fffab3246f18c9f9458P.jpg)


## 小程序生产预览(微信小程序搜索：婚笺 或 扫码预览)
![](https://ae01.alicdn.com/kf/U5a493491db2a422987644bb958ea2734w.jpg)


## 安装教程
```javascript
# 1. 克隆项目
git clone https://gitee.com/zijun2030/hun-jian.git
# 2. 进入微信开发者工具运行
```

## 说明事项
1. 微信开发者工具需填入自己的AppID
2. 目前这里只包含本地的云函数和页面代码，云函数、云存储、云数据库暂未上传
3. 项目依赖：腾讯位置服务，需开通
4. 依据目前本地代码，可以实现云数据推导，实现项目完整功能
5. 如果不想自己推导接口字段，下方会有云数据库的导出文件，导入即可（云存储可自己处理）
6. 如开发过程遇到什么问题，微信联系我可提供有偿服务

## 云数据库
```json
{"_id":"17453ede609b2f50091e5bc6622551b0","date1":"2022-01-01","time":"09:00","photos":["cloud://dev-0gvrrg8192ed8dad.6465-dev-0gvrrg8192ed8dad-1257262755/photos/2021-05-12_095656.png","cloud://dev-0gvrrg8192ed8dad.6465-dev-0gvrrg8192ed8dad-1257262755/photos/2021-05-12_095729.png","cloud://dev-0gvrrg8192ed8dad.6465-dev-0gvrrg8192ed8dad-1257262755/photos/2021-05-12_095808.png","cloud://dev-0gvrrg8192ed8dad.6465-dev-0gvrrg8192ed8dad-1257262755/photos/2021-05-12_100141.png","cloud://dev-0gvrrg8192ed8dad.6465-dev-0gvrrg8192ed8dad-1257262755/photos/2021-05-12_100208.png","cloud://dev-0gvrrg8192ed8dad.6465-dev-0gvrrg8192ed8dad-1257262755/photos/2021-05-12_100248.png"],"groom":"李雷","bride":"韩梅梅","restaurant":"深圳福田香格里拉大酒店","cover":"cloud://dev-0gvrrg8192ed8dad.6465-dev-0gvrrg8192ed8dad-1257262755/cover/2021-05-12_145330.png","address":"广东省深圳福田香格里拉大酒店","invitationBg":"cloud://dev-0gvrrg8192ed8dad.6465-dev-0gvrrg8192ed8dad-1257262755/photos/2021-05-12_095656.png","lat":22.542043,"lon":114.063501,"hotel":"深圳福田香格里拉大酒店","weChatNoUrl":"cloud://dev-0gvrrg8192ed8dad.6465-dev-0gvrrg8192ed8dad-1257262755/others/20210512133745.jpg","shareCoverImg":"cloud://dev-0gvrrg8192ed8dad.6465-dev-0gvrrg8192ed8dad-1257262755/share/2021-05-12_135639.png","media":{"singer":"蔡依林&陶喆","title":"今天你要嫁给我","url":"cloud://dev-0gvrrg8192ed8dad.6465-dev-0gvrrg8192ed8dad-1257262755/media/jtnyjgw.mp3"},"miniProgramUrl":"cloud://dev-0gvrrg8192ed8dad.6465-dev-0gvrrg8192ed8dad-1257262755/others/gh_9e9d62aadfba_430.jpg","photoInterVal":3000.0}
```










