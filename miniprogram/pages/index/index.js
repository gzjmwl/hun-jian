const app = getApp()

Page({
  data: {
    canOpen: true,
    hasClosed: false,
    baseinfo: {}
  },

  // 关闭进入时邀请信封
  handleOnClose() {
    this.setData({
      canOpen: false,
      hasClosed: true
    })
  },
  // 打开邀请信封
  openLetter() {
    this.setData({
      canOpen: true
    })
  },

  onLoad: function () {
    const {
      baseinfo
    } = app.globalData;
    this.setData({
      baseinfo: baseinfo || {}
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '诚邀参加我的婚礼',
      path: '/pages/index/index',
      imageUrl: this.data.baseinfo.shareCoverImg
    }
  },

  onShareTimeLine() {
    return {
      title: '诚邀参加我的婚礼',
      imageUrl: this.data.baseinfo.shareCoverImg,
    }
  }
})