// components/letter/letter.js
const app = getApp();

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    canOpen: {
      type: Boolean,
      value: true
    },
    baseinfo: {
      type: Object,
      value: {}
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    // canOpen: true,
    isOpening: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 打开邀请函
    openInvitation() {
      this.setData({
        isOpening: true
      })
    },
    // 关闭邀请函
    closeInvitation() {
      this.setData({
        isOpening: false
      })
      setTimeout(() => {
        this.triggerEvent('onClose');
      }, 600)
    },
    // 跳转到邀请首页
    jumpToInvitation() {
      wx.switchTab({
        url: '/pages/invitation/invitation'
      })
    }
  },

  lifetimes: {
    async attached() {
      if(!this.baseinfo) {
        wx.showLoading();
        const res = await app.getBaseInfo();
        this.setData({
          baseinfo: res
        })
        wx.hideLoading();
        app.globalData.baseinfo = res;
      }
    }
  }
})
