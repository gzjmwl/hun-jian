Component({
  /**
   * 组件的属性列表
   */
  properties: {
      count: {
        type: Number,
        value: 1
      },
      double: {
        type: Boolean,
        value: false
      }
  },

  /**
   * 组件的初始数据
   */
  data: {
    countArray: []
  },

  observers: {
    count(v) {
      this.setCount(v)
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    setCount(v) {
      this.setData({
        countArray: new Array(v).fill(1)
      })
    }
  },

  lifetimes: {
    attached() {
      this.setCount(this.properties.count)
    }
  }

})
